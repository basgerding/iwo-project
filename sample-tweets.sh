#!/bin/bash

# These commands access the tweets from 1 March 2017 12:00 and counts them with the command wc -l.
echo "Amount of tweets:"
zless /net/corpora/twitter2/Tweets/2017/03/20170301\:12.out.gz | /net/corpora/twitter2/tools/tweet2tab -i text | wc -l
# These commands access the tweets from 1 March 2017 12:00, sorts them and only returns the unique ones with uniq. Then it counts the lines with wc -l.
echo "Amount of unique tweets:"
zless /net/corpora/twitter2/Tweets/2017/03/20170301\:12.out.gz | /net/corpora/twitter2/tools/tweet2tab -i text | sort | uniq | wc -l
# These commands access the tweets from 1 March 2017 12:00, sorts them and only returns the unique ones with uniq. Then it searches for the tweets that begin with RT by using grep '^RT' and then it counts the lines with wc -l.
echo "Amount of retweets out of the unique tweets:"
zless /net/corpora/twitter2/Tweets/2017/03/20170301\:12.out.gz | /net/corpora/twitter2/tools/tweet2tab -i text | sort | uniq | grep '^RT' | wc -l
# These commands access the tweets from 1 March 2017 12:00, sorts them and only returns the unique ones with uniq. Then it searches for the tweets that don't start with RT by using grep -v '^RT' and then it shows the first 20 by using the command head -20.
echo "The first 20 unique tweets that are not retweets:"
zless /net/corpora/twitter2/Tweets/2017/03/20170301\:12.out.gz | /net/corpora/twitter2/tools/tweet2tab -i text | sort | uniq | grep -v '^RT' | head -20

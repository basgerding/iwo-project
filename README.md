# README #

Inleiding Wetenschappelijk Onderzoek

### What is this repository for? ###

This repository is used for the course Inleiding Wetenschappelijk Onderzoek. It contains a shellscript called sample-tweets.sh that accesses the in-house RUG Twitter data. 
This particular shellscript will access the twitter file from 1 March 2017 12:00. It will answer the questions asked in the assignment.